# NOTA

Author: **Jan Uhlík (uhlajs)**

## Credits

All icons are taken from [Icons8](https://icons8.com) and are distributed under a [Creative Commons Attribution-NoDerivs 3.0 Unported](https://creativecommons.org/licenses/by-nd/3.0/) license.

## Notes

To get units name:
1. Select unit
2. Open Console `F9`
3. Run: `UnitDefs[Spring.GetUnitDefID(Spring.GetSelectedUnits()[1])].name`

Get units in locations rectangle
- `Spring.GetUnitsInRectangle()`

## TTDR TODO

- Change icon for `ttdr` behavior (DONE)
- Paralelization (use more `atlas`es for resque)
    - Follow dude example
    - Solve problem when we runout of atlases
- Solve Lag with `grid` computation
    - Remove `table.insert` (see: https://springrts.com/wiki/Lua_Performance#TEST_12:_Adding_Table_Items_.28table.insert_vs._.5B_.5D.29)
    - Compute `grid` in multiple frames (return table with unfinished computation and `luaCondition` on finished)
    - Change algorithm (DONE)

## CTP2 TODO

1. Get hill infos (DONE)
1. Split units to groups (DONE)
2. Move units to first 3 hills (DONE)
3. Exit mission (DONE)
3. Attact last hill in formation (DONE)
4. Load units with `armthovr` (SKIPED)
5. Unload units in 4th hill (SKIPED)

