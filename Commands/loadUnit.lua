function getInfo()
    return {
        onNoUnits = SUCCESS,
        tooltip = "",
        parameterDefs = {
            {
                name = "transporterID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "unitID",
            },
            {
                name = "passengerID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "unitID",
            },
        }
    }
end

local function ClearState(self)
    self.initialization = false
end

local function checkFail(self, parameter)
    return (not self.initialization and Sensors.numberOfEntries(Spring.GetUnitIsTransporting(parameter.transporterID)) ~= 0)  -- Already loaded at the beginning
            or
            Spring.GetUnitIsDead(parameter.transporterID) -- trasporter was destroyed
end

local function checkSuccess(self, parameter)
    return (self.initialization and Sensors.numberOfEntries(Spring.GetUnitIsTransporting(parameter.transporterID)) ~= 0) -- Alreade unloaded
end

function Run(self, units, parameter)
    if checkFail(self, parameter) then
        return FAILURE
    elseif checkSuccess(self, parameter) then
        return SUCCESS
    end
    if Spring.GetUnitTransporter(parameter.passengerID) == parameter.transporterID then
        return SUCCESS
    end

    if not self.initialization then
        self.initialization = true
        Spring.GiveOrderToUnit(
            parameter.transporterID,
            CMD.LOAD_UNITS,
            {parameter.passengerID}, --{x,y,z,250},
            {} -- {"shift"}
        )
    end
    return RUNNING
end

function Reset(self)
    ClearState(self)
end