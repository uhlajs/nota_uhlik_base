function getInfo()
  return {
      onNoUnits = FAILURE,
      tooltip = "",
      parameterDefs = {
          {
              name = "unit",
              variableType = "expression",
              componentType = "editBox",
              defaultValue = "unitID",
          },
          {
              name = "path",
              variableType = "expression",
              componentType = "editBox",
              defaultValue = "path",
          },
          {
            name = "threshold",
            variableType = "expression",
            componentType = "editBox",
            defaultValue = "200",
        },
      }
  }
end


local function ClearState(self)
  self.initialization = false
end

local function checkFail(self, parameter)
  return false
end

local function distance(a, b)
	return math.sqrt(math.pow(a.x - b.x, 2) + math.pow(a.z - b.z, 2))
end

local function checkSuccess(self, parameter)
  local x, y, z = Spring.GetUnitPosition(parameter.unit)
  return distance(parameter.path[#parameter.path], Vec3(x, y, z)) < parameter.threshold
end

function Run(self, units, parameter)
  if checkFail(self, parameter) then
      return FAILURE
  elseif checkSuccess(self, parameter) then
      return SUCCESS
  end

  if not self.initialization then
      self.initialization = true
      for _, point in ipairs(parameter.path) do
        Spring.GiveOrderToUnit(
            parameter.unit,
            CMD.MOVE,
            point:AsSpringVector(), 
            {"shift"}
        )
      end
  end
  return RUNNING
end

function Reset(self)
  ClearState(self)
end