function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "formation", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<relative formation>",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 500
local THRESHOLD_DEFAULT = 40

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.thresholds = {}
	self.lastUnitsPosition = {}
	
	if self.units ~= nil then
		for i = 1, #self.units do
			self.lastUnitsPosition[i] = Vec3(0,0,0)
			self.thresholds[i] = THRESHOLD_DEFAULT
		end
	end
end

function Run(self, units, parameter)
	if parameter.position == nil then
		return SUCCESS
	end
	
	self.units = units or {}
	self.lastUnitsPosition = self.lastUnitsPosition or {}
	self.thresholds = self.thresholds or {}
	local position = parameter.position -- Vec3
	local formation = parameter.formation -- array of Vec3
	local fight = parameter.fight -- boolean
	
	--Spring.Echo(dump(parameter.formation))
	
	-- validation
	if (#self.units > #formation) then
		Logger.warn("formation.move", "Your formation size [" .. #formation .. "] is smaller than needed for given count of units [" .. #units .. "] in this group.") 
		return FAILURE
	end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	local pointmanPosition = position
	
	-- check pointman success
	-- THIS LOGIC IS TEMPORARY, NOT CONSIDERING OTHER UNITS POSITION
	local ret_value = SUCCESS
	for i=1, #self.units do
		local pointX, pointY, pointZ = SpringGetUnitPosition(self.units[i])
		local unitPosition = Vec3(pointX, pointY, pointZ)
		
		-- threshold of unit success
		if (unitPosition == self.lastUnitsPosition[i]) then 
			self.thresholds[i] = self.thresholds[i] + THRESHOLD_STEP 
		else
			self.thresholds[i] = THRESHOLD_DEFAULT
		end
		self.lastUnitsPosition[i] = unitPosition

		local thisUnitWantedPosition = pointmanPosition + formation[i]
		if (unitPosition:Distance(thisUnitWantedPosition) > self.thresholds[i]) then
			ret_value = RUNNING
		end
		
		SpringGiveOrderToUnit(self.units[i], cmdID, thisUnitWantedPosition:AsSpringVector(), {})
		--SpringGiveOrderToUnit(self.units[i], cmdID, {x,y,z,r}, {}) -- x,y,z position, r radius
	end
	return ret_value
end


function Reset(self)
	ClearState(self)
end
