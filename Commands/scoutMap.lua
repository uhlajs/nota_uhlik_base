function getInfo()
    return {
        onNoUnits = FAILURE,
        tooltip = "",
        parameterDefs = {
            {
                name = "scoutUnits",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "scoutUnitsIDs",
            },
            {
                name = "direction",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "\"south\"",
            },
        }
    }
end

local function ClearState(self)
    self.initialization = false
end

local function splitRegion(region_size, count)
    offset = 100
    step = (region_size - 2*offset) / count
    coordinates = {}
    for i=1,count do
        table.insert(coordinates, offset + i*step)
    end
    return coordinates
end

local function getMapInfo(direction, edge_offset)
    if direction == "south" then
        region_size = Game.mapSizeX
        secondary_coordinate = Game.mapSizeZ - edge_offset
        is_primary_x = true
    elseif direction == "north" then
        region_size = Game.mapSizeX
        secondary_coordinate = edge_offset
        is_primary_x = true
    elseif direction == 'east' then
        region_size = Game.mapSizeZ
        secondary_coordinate = Game.mapSizeX - edge_offset
        is_primary_x = false
    elseif direction == 'east' then
        region_size = Game.mapSizeZ
        secondary_coordinate = edge_offset
        is_primary_x = false
    else
        Logger.error("Unknown direction" .. direction)
    end
    return region_size, secondary_coordinate, is_primary_x
end

local function getTargetPositions(count, direction)
    region_size, secondary_coordinate, is_primary_x = getMapInfo(direction, 150)
    primary_coordinates = splitRegion(region_size, count)
    positions = {}
    for i=1,count do
        if is_primary_x then
            table.insert(positions, Vec3(primary_coordinates[i], 0, secondary_coordinate))
        else
            table.insert(positions, Vec3(secondary_coordinate, 0 ,primary_coordinates[i]))
        end
    end
    return positions
end

local function getOtherDirection(direction)
    if direction == "north" then
        return "south"
    elseif direction == "south" then
        return "north"
    elseif direction == "east" then
        return "west" 
    elseif direction == "west" then
        return "east" 
    end
end

function Run(self, units, parameter)
    if #parameter.scoutUnits == 0 then
        return FAILURE
    end
    if not self.initialization then
        self.initialization = true
        endPositions = getTargetPositions(#parameter.scoutUnits, parameter.direction)
        startPositions = getTargetPositions(#parameter.scoutUnits, getOtherDirection(parameter.direction))
        for i=1,#parameter.scoutUnits do
            Spring.GiveOrderToUnit(
                parameter.scoutUnits[i],
                CMD.MOVE,
                startPositions[i]:AsSpringVector(),
                {} -- {"shift"}
            )
            Spring.GiveOrderToUnit(
                parameter.scoutUnits[i],
                CMD.MOVE,
                endPositions[i]:AsSpringVector(),
                {"shift"}
            )
            --[[ Spring.GiveOrderToUnit(
                parameter.scoutUnits[i],
                CMD.MOVE,
                {Spring.GetUnitPosition(parameter.scoutUnits[i])},
                {"shift"}
            ) ]]
        end
        bb.scout_positions = positions
    end
    
    return RUNNING
end

function Reset(self)
    ClearState(self)
end