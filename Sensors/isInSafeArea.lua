local sensorInfo = {
	name = "isInSafeArea",
	desc = "Return true if unit is in safe area.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetUnitTransporter = Spring.GetUnitTransporter

local function distance(a, b)
	return math.sqrt(math.pow(a.x - b.x, 2) + math.pow(a.y - b.y, 2) + math.pow(a.z - b.z, 2))
end

local center = Sensors.core.MissionInfo().safeArea.center
local radius = Sensors.core.MissionInfo().safeArea.radius

return function(unitID)
  local x, y, z = Spring.GetUnitPosition(unitID)
  local pos = Vec3(x, y, z)

  return distance(center, pos) <= radius and GetUnitTransporter(unitID) == nil
end