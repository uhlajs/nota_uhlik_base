local sensorInfo = {
	name = "playground",
	desc = "Return wind direction.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function(circle, radius)
	local formation = {}
	local x, z
	local n = #units
	local x0 = global.center.x
	local z0 = global.center.z
	
	local da = 2.0 * math.pi/n
	local a = 0.0
	for i = 0, n - 1 do
		x = radius * math.cos(a)
		z = radius * math.sin(a)
		-- draw here using x,y
		a = a + da
		
		formation[i + 1] = Vec3(x, global.center.y, z)
	end
		
	return formation

end