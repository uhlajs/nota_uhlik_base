local sensorInfo = {
	name = "filterUnits",
	desc = "Return the visible units in radius.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function(listOfUnits)
	local filteredUnits = {}
	for i=1, #listOfUnits do
		local unitID = listOfUnits[i]
		local unitDefID = Spring.GetUnitDefID(unitID)
		local unitDefTable = UnitDefs[unitDefID]
		if unitDefTable.canFly then
			filteredUnits[#filteredUnits + 1] = unitID
		end
	end
	
	return filteredUnits
end