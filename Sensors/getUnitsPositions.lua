local sensorInfo = {
	name = "getUnitsPositions",
	desc = "Return units positions.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current units positions
return function(units)
	positions = {}
	for i = 1, #units do
		local x, y, z = Spring.GetUnitPosition(units[i])
		positions[i] = Vec3(x, y, z)
	end
	return positions
end