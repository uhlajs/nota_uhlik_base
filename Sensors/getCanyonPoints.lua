local sensorInfo = {
	name = "getCanyonPoints",
	desc = "Maps terrain as grid with granularity of certain height (between minHeight and maxHeight). Storing points in list with unified height (= maxHeight).",
	author = "Ondrelord",
	date = "2018-04-16",1,
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = math.huge
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

--speedups
local GetGroundHeight = Spring.GetGroundHeight

-- @description 
return function(granularity, minHeight, maxHeight)
	local canyonSamples = {}

	for x = 0,  Game.mapSizeX, granularity do
		for z = 0,  Game.mapSizeZ, granularity do
			y = GetGroundHeight(x, z)
			
			if y >= minHeight and y < maxHeight then
				table.insert(canyonSamples, Vec3(x, y, z))
			end
		end
	end
	
	return canyonSamples
end