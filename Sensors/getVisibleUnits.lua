local sensorInfo = {
	name = "getVisibleUnits",
	desc = "Return the visible units in radius.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind
local GetUnitsPosition = Spring.GetUnitPosition
local GetUnitTeam = Spring.GetUnitTeam
local GetUnitsInCylinder = Spring.GetUnitsInCylinder

return function(unitID, radius)
	local x, y, z = GetUnitPosition(unitID)
	local teamID = GetUnitTeam(unitID)
	return GetUnitsInCylinder(x, z, radius, teamID)
end