local sensorInfo = {
	name = "getGroupsForCPT2",
	desc = "Get groups for CPT2 mission.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = math.huge -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function getUnitName(unitID)
    return UnitDefs[Spring.GetUnitDefID(unitID)].name
end

function createPWGroup(units)
    pwGroup = {}
    restUnits = {}
    done = false
    for i = 1, #units do
        if getUnitName(units[i]) == 'armpw' and not done then
            pwGroup[units[i]] = 1 -- formation index
            done = true
        else
            table.insert(restUnits, units[i])
        end
    end

    return pwGroup, restUnits
end

function createWarGroup(units)
    warGroup = {}
    restUnits = {}
    index = 1

    -- Force to be bear as first
    for i = 1, #units do
        if getUnitName(units[i]) == 'armthovr' then
            warGroup[units[i]] = index -- formation index
            index = index + 1
        else
            restUnits[#restUnits + 1] = units[i]
        end
    end

    for i = 1, #restUnits do
        if getUnitName(restUnits[i]) == 'armwar' then
            warGroup[restUnits[i]] = index -- formation index
            index = index + 1        
        end
    end

    return warGroup
end


-- @description return current wind statistics
return function(units)
    if #units <= 0 then
        return nil
    end

    pwGroup1, restUnits = createPWGroup(units)
    pwGroup2, restUnits = createPWGroup(restUnits)
    pwGroup3, restUnits = createPWGroup(restUnits)
    warGroup = createWarGroup(restUnits)

    return {
        pwGroups = {
            pwGroup1,
            pwGroup2,
            pwGroup3,
        },
        warGroup = warGroup,
    }
end