local sensorInfo = {
	name = "numberOfEntries",
	desc = "Return number of entries in table.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(T)
    local count = 0
    for _ in pairs(T) do count = count + 1 end
    return count
end