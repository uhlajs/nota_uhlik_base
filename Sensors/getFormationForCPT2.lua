local sensorInfo = {
	name = "getFormationForCPT2",
	desc = "Get group formation for CPT2",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = math.huge -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(units)
    return {
        Vec3(0,0,0),
        Vec3(-32, 0, -16),
        Vec3(-32, 0, 0),
        Vec3(-32, 0, 16),
    }
end