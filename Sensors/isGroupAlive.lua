local sensorInfo = {
	name = "isGroupAlive",
	desc = "Return true if all units in group are alive.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local ValidUnitID = Spring.ValidUnitID

local function isGroupAlive(group)
  if group == nil then 
    return false 
  end
	
	for _, unitID in ipairs(group.units) do
		local isAlive = ValidUnitID(unitID)
		if not isAlive then
			return false
		end
	end
	
	return true
end

return function(group)
	return isGroupAlive(group)
end