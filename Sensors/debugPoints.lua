local sensorInfo = {
	name = "debugPoints",
	desc = "Show points on map.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 0 -- cache per frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(points, radius, ttl, color, width)
  local radius = radius or 10
  local ttl = ttl or 100
  local color = color or {0, 1, 0, 1}
  local width = width or 3

  for i, point in ipairs(points) do
      if (Script.LuaUI('debugLocation_update')) then
        Script.LuaUI.debugLocation_update(
          i, -- key
          {	-- data
            location = point,
            radius = radius,
            timeToLive = ttl,
            color = color,
            width = width,
          }
			)
		end
	end
end