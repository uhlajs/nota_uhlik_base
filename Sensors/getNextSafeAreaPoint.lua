local sensorInfo = {
	name = "getNextSafeAreaPoint",
	desc = "Get next safe are point which is empty.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetUnitsInCylinder = Spring.GetUnitsInCylinder

return function(transporterID, passangerID)
  local safeAreaPoints = Sensors.getSafeAreaPoints()
  
  local records = {}
  for _, point in ipairs(safeAreaPoints) do
    if Sensors.isEmptyFor(point, transporterID, passangerID) then
      return point
    end
  end
end