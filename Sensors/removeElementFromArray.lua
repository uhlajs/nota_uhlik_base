local sensorInfo = {
	name = "removeElementFrom",
	desc = "Remove element from array.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(array, element)
  local index = nil
  for i, e in ipairs(array) do
    if element == e then
      index = i
    end
  end
  if index ~= nil then
    table.remove(array, index)
  end
end