local sensorInfo = {
	name = "getNextResqueUnit",
	desc = "Get next resque unit which is alive and not in save area.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local ValidUnitID = Spring.ValidUnitID

local function distance(a, b)
	return math.sqrt(math.pow(a.x - b.x, 2) + math.pow(a.y - b.y, 2) + math.pow(a.z - b.z, 2))
end

local center = Sensors.core.MissionInfo().safeArea.center
local radius = Sensors.core.MissionInfo().safeArea.radius

return function(targetGroups, transporter)
	local transporterID = transporter.units[1]
	local x, y, z = Spring.GetUnitPosition(transporterID)
	local transporterPos = Vec3(x, y, z)
	local closestUnit = nil
	local closestUnitDistance = math.huge

	for _, group in ipairs(targetGroups) do
		local unitID = group.units[1]
    local x, y, z = Spring.GetUnitPosition(unitID)
		local pos = Vec3(x, y, z)
		local dist = distance(pos, transporterPos)
    if ValidUnitID(unitID) and group.status == "idle" and distance(center, pos) > radius and dist < closestUnitDistance then
			closestUnit = group
			closestUnitDistance = dist
    end
	end
	return closestUnit
end