local sensorInfo = {
	name = "isEmptyFor",
	desc = "Point is empty for unit.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetUnitsInCylinder = Spring.GetUnitsInCylinder

return function(point, transporterID, passangerID, radius)
    local radius = radius or 80

    local unitsInCylinder = GetUnitsInCylinder(point.x, point.z, radius)
    for _, unitID in ipairs(unitsInCylinder) do
      if unitID ~= transporterID and unitID ~= passangerID then
        return false
      end
    end
    return true
end