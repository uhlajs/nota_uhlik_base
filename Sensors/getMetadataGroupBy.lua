local sensorInfo = {
	name = "getMetadataGroupBy",
	desc = "Returns groups with `countInOneGroup` units with metadata information.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(troops, countInOneGroup)
  local groups = {}
	for i=1, #troops, countInOneGroup do
		local newGroupIndex = #groups + 1
		groups[newGroupIndex] = {}
		for j=i, i+countInOneGroup-1 do
			groups[newGroupIndex][#groups[newGroupIndex] + 1] = troops[j]
		end
	end
	
	local groupsWithMetadata = {}
	for i=1, #groups do
		local index = #groupsWithMetadata + 1
		groupsWithMetadata[index] = {
      index = index,
      status = "idle",
      units = groups[i],
		}
	end
	
	return groupsWithMetadata
end