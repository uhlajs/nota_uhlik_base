local sensorInfo = {
	name = "getSafePoints",
	desc = "Return points which are out of range of other team.",
	author = "PatrikValkovic, uhlajs",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local getEnemyPositions = Sensors.getEnemyPositions
local GetPositionLosState = Spring.GetPositionLosState
local GetGroundHeight = Spring.GetGroundHeight

local function distance(a, b)
	return math.sqrt(math.pow(a.x - b.x, 2) + math.pow(a.y - b.y, 2) + math.pow(a.z - b.z, 2))
end
-- @description 
return function(granularity, maxEnemyRange)
	local safePoints = {}
	local enemyPositions = getEnemyPositions()

	for x = 0,  Game.mapSizeX, granularity do
		for z = 0,  Game.mapSizeZ, granularity do
			point = Vec3(x, GetGroundHeight(x, z), z)
			safe = true

			for _, enemyPosition in pairs(enemyPositions) do
				local dist = distance(point, enemyPosition)
				if dist <= maxEnemyRange then
					safe = false
        end
			end
			
			if safe then
				table.insert(safePoints, point)
			end
		end
	end
	return safePoints
end