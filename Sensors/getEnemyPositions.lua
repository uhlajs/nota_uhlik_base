local sensorInfo = {
	name = "getEnemyPositions",
	desc = "Returns positions of the enemy units.",
	author = "uhlajs",
	date = "2020-05-23",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = 0 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetUnitPosition = Spring.GetUnitPosition
local EnemyUnits = Sensors.core.EnemyUnits

return function()
  local enemyUnits = EnemyUnits()
  local enemyPositions = {}
  for _, unitid in pairs(enemyUnits) do
    local x, y, z = GetUnitPosition(unitid)
    table.insert(enemyPositions, Vec3(x, y, z))
  end
  return enemyPositions
end
