local sensorInfo = {
	name = "pickGroupByStatus",
	desc = "Return first groups with given status.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local ValidUnitID = Spring.ValidUnitID

local function isGroupAlive(group)
  if group == nil then 
    return false 
  end
	
	for _, unitID in ipairs(group.units) do
		local isAlive = ValidUnitID(unitID)
		if not isAlive then
			return false
		end
	end
	
	return true
end

return function(groups, status)
	for _, group in ipairs(groups) do
    if group.status == status and isGroupAlive(group) then
      Spring.Echo("TU")
			return group
		end
	end
	
	return nil
end