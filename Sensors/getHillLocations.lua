local sensorInfo = {
	name = "getHillLocations",
	desc = "Get get location of all hills.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(stepX, stepY, treshold)
    hillLocations = {}
    for i = 1, Game.mapSizeX, stepX do
        for j = 1, Game.mapSizeZ, stepY do
            height = Spring.GetGroundHeight(i, j)
            if height >= treshold then
                table.insert(hillLocations, Vec3(i, height, j)) -- use formation.move format
            end
        end
    end
	return hillLocations
end