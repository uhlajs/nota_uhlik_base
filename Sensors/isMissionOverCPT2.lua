local sensorInfo = {
	name = "isMissionOverCPT2",
	desc = "Return true if mission is over.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(numberOfHills)
    info = Sensors.core.MissionInfo()

    finish = true
    for i = 1, numberOfHills do
        finish = finish and info.areasOccupied[i]
    end
    
    return finish
end