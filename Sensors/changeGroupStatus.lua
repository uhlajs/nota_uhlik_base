local sensorInfo = {
	name = "changeGroupStatus",
	desc = "Change group status.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}


local EVAL_PERIOD_DEFAULT = -1 -- instant, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(group, status)
	group.status = status
end