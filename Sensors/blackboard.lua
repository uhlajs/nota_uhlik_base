local sensorInfo = {
	name = "blackboard",
	desc = "Get some debug information.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


-- @description return current wind statistics
return function()
    return Sensors.core.MissionInfo()
end