local sensorInfo = {
	name = "getSafeAreaPoints",
	desc = "Get location of safe unload locations.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(step)
    local step = step or 100

    local missionInfo = Sensors.core.MissionInfo()
    local safeArea = missionInfo.safeArea
    local offset = 50
    local a = safeArea.radius / math.sqrt(2)
    local base = safeArea.center - Vec3(a, 0, a)

    local points = {}

    for i = offset, 2*a - offset, step do
        for j = offset, 2*a - offset, step do
            local point = base + Vec3(i, 0, j)
            table.insert(points, point)
        end
    end
    return points
end