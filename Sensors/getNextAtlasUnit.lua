local sensorInfo = {
	name = "getNextAtlasUnit",
	desc = "Get next alive atlas unit.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetUnitIsDead = Spring.GetUnitIsDead

return function(atlasGroup)
    for _, unitID in ipairs(atlasGroup) do
      if GetUnitIsDead(unitID) == false then
        return unitID
      end
    end
end