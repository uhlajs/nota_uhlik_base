local sensorInfo = {
	name = "mergeArrays",
	desc = "Merge two arrays in one without duplicities.",
	author = "uhlajs",
	date = "2020-05-23",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- no cachining

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(firstArray, secondArray)
  newArray = {}
  hash = {}

  for _, v in ipairs(firstArray) do 
    if not hash[v] then
      table.insert(newArray, v)
      hash[v] = true
    end
  end

  for _, v in ipairs(secondArray) do
    if not hash[v] then
      table.insert(newArray, v)
      hash[v] = true
    end
  end
  return newArray
end
