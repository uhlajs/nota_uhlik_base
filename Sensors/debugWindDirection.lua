local sensorInfo = {
	name = "windDirectionDebug",
	desc = "Show wind direction.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(factor)
	for i = 1, #units do
		local unitID = units[i]
		if (Script.LuaUI('debugWindDirection_update')) then
			Script.LuaUI.debugWindDirection_update(
				unitID, -- key
				{	-- data
					infoCall = function() return Sensors.nota_uhlik_base.getWindDirection(factor) end,
					--timeToLive=math.inf,
				}
			)
		end
	end
end