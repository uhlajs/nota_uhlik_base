local sensorInfo = {
	name = "circleFormation",
	desc = "Return list of circle formation relative to center with given radius.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function(radius)
	local formation = {}	
	local da = 2.0 * math.pi/#units
	local a = 0.0
	
	for i = 0, #units - 1 do
		formation[i + 1] = Vec3(radius * math.cos(a), 0, radius * math.sin(a))
		a = a + da
	end
		
	return formation

end