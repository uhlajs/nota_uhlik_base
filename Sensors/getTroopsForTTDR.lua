local sensorInfo = {
	name = "getGroupsForCPT2",
	desc = "Get groups for CPT2 mission.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = math.huge -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local GetMyTeamID = Spring.GetMyTeamID
local GetTeamUnits = Spring.GetTeamUnits

local function getUnitName(unitID)
    return UnitDefs[Spring.GetUnitDefID(unitID)].name
end

local function isIn(element, array)
    for _, e in ipairs(array) do
        if e == element then
            return true
        end
    end
    return false
end

local function filterUnitByName(units, unitNames)
    group = {}
    restUnits = {}
    for i = 1, #units do
        if isIn(getUnitName(units[i]), unitNames) then
            table.insert(group, units[i])
        else
            table.insert(restUnits, units[i])
        end
    end

    return group, restUnits
end

return function(units)
    local armatlas, others = filterUnitByName(units, {"armatlas"})
    local armpeep, others = filterUnitByName(others, {"armpeep"})
    -- Find all resque units
    local targetUnits, _ = filterUnitByName(GetTeamUnits(GetMyTeamID()), {"armmllt", "armham", "armbox", "armbull", "armrock"})
   
    return {
        armatlas = armatlas,
        armpeep = armpeep,
        others = others,
        targetUnits = targetUnits,
    }
end