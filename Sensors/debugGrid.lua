local sensorInfo = {
	name = "debugGrid",
	desc = "Show grid on map.",
	author = "uhlajs",
	date = "2020-04-28",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 0 -- cache per frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(grid, radius, ttl, color, width)
  radius = radius or 10
  ttl = ttl or 100
  color = color or {0, 1, 0, 1}
	width = width or 3

  local ID = 0
  for startPoint, neig in pairs(grid) do
		for _, endPoint in ipairs(neig) do
			ID = ID + 1
			if (Script.LuaUI('debugLine_update')) then
				Script.LuaUI.debugLine_update(
					ID, -- key
					{	-- data
					startPos = startPoint,
					endPos = endPoint,
          timeToLive = ttl,
          color = color,
          width = width,
				})
			end
		end
	end
	Spring.Echo(ID)
end