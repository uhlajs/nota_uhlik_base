moduleInfo = {
	name = "widgetLocation",
	desc = "Location debug with time out.",
	author = "uhlajs",
	date = "2019-04-16",
	license = "MIT",
	layer = math.huge, -- <= tady se nastavuje nejnižší priorita widgetu
    enabled = false --  <= tady, to je vypnutí
}

function widget:GetInfo()
	return moduleInfo
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "stringExt")
Vec3 = attach.Module(modules, "vec3")

local spEcho = Spring.Echo
local spAssignMouseCursor = Spring.AssignMouseCursor
local spSetMouseCursor = Spring.SetMouseCursor
local spGetGroundHeight = Spring.GetGroundHeight
local spTraceScreenRay = Spring.TraceScreenRay
local spGetUnitPosition = Spring.GetUnitPosition
local glColor = gl.Color
local glRect = gl.Rect
local glTexture	= gl.Texture
local glDepthTest = gl.DepthTest
local glBeginEnd = gl.BeginEnd
local glPushMatrix = gl.PushMatrix
local glPopMatrix = gl.PopMatrix
local glTranslate = gl.Translate
local glDrawGroundCircle = gl.DrawGroundCircle
local glText = gl.Text
local glLineWidth = gl.LineWidth
local glLineStipple = gl.LineStipple
local glVertex = gl.Vertex
local GL_LINE_STRIP = GL.LINE_STRIP
local TextDraw = fontHandler.Draw
local max = math.max
local min = math.min

local instances = {}

local function Update(lineID, lineData)
	instances[lineID] = lineData
end

function widget:Initialize()
	widgetHandler:RegisterGlobal('debugLocation_update', Update)
end

function widget:GameFrame(n)
end

function widget:DrawWorld()
	for id, instanceData in pairs(instances) do
		if (instanceData.location ~= nil and instanceData.radius ~= nil and instanceData.color ~= nil and instanceData.width ~= nil and instanceData.timeToLive > 0) then
			-- Decrease TOL
			instanceData.timeToLive = instanceData.timeToLive - 1
			glColor(instanceData.color[1], instanceData.color[2], instanceData.color[3], instanceData.color[4])
			glLineWidth(instanceData.width)
			
			-- See: https://springrts.com/wiki/Lua_OpenGL_Api
			glDrawGroundCircle(instanceData.location.x, instanceData.location.y, instanceData.location.z, instanceData.radius, 20)
		end
	end
	glColor(1, 0, 0, 1)
end