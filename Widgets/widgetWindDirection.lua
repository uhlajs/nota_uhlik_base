moduleInfo = {
	name = "widgetWindDirection",
	desc = "wind direction debug",
	author = "uhlajs",
	date = "2019-04-16",
	license = "MIT",
	layer = math.huge, -- <= tady se nastavuje nejnižší priorita widgetu
    enabled = false --  <= tady, to je vypnutí
}

function widget:GetInfo()
	return moduleInfo
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "stringExt")
Vec3 = attach.Module(modules, "vec3")

local spEcho = Spring.Echo
local spAssignMouseCursor = Spring.AssignMouseCursor
local spSetMouseCursor = Spring.SetMouseCursor
local spGetGroundHeight = Spring.GetGroundHeight
local spTraceScreenRay = Spring.TraceScreenRay
local spGetUnitPosition = Spring.GetUnitPosition
local glColor = gl.Color
local glRect = gl.Rect
local glTexture	= gl.Texture
local glDepthTest = gl.DepthTest
local glBeginEnd = gl.BeginEnd
local glPushMatrix = gl.PushMatrix
local glPopMatrix = gl.PopMatrix
local glTranslate = gl.Translate
local glText = gl.Text
local glLineWidth = gl.LineWidth
local glLineStipple = gl.LineStipple
local glVertex = gl.Vertex
local GL_LINE_STRIP = GL.LINE_STRIP
local TextDraw = fontHandler.Draw
local max = math.max
local min = math.min

local instances = {}

local function Update(lineID, lineData)
	instances[lineID] = lineData
end

function widget:Initialize()
	widgetHandler:RegisterGlobal('debugWindDirection_update', Update)
end

function widget:GameFrame(n)
end

function widget:DrawWorld()
	for unitID, instanceData in pairs(instances) do
		if (instanceData.infoCall ~= nil) then
		--if (instanceData.infoCall ~= nil and instanceData.timeToLive > 0) then
			-- Decrease TOL
			--instanceData.timeToLive = instanceData.timeToLive - 1
		
			local function Line(a, b)
				glVertex(a[1], a[2], a[3])
				glVertex(b[1], b[2], b[3])
			end
			
			local function DrawLine(a, b)
				glLineStipple(false)
				glLineWidth(5)
				glBeginEnd(GL_LINE_STRIP, Line, a, b)
				glLineStipple(false)
			end
			
			local x, y, z = Spring.GetUnitPosition(unitID)
			local startPos = Vec3(x, y, z)
			local windDirection = instanceData.infoCall()
			local endPos = startPos + windDirection
			glColor(0, 0, 1, 0.5)
			
			DrawLine(
				{
					startPos.x,
					startPos.y,
					startPos.z
				}, 
				{
					endPos.x,
					endPos.y,
					endPos.z
				}
			)
		end
	end
	glColor(1, 0, 0, 1)
end